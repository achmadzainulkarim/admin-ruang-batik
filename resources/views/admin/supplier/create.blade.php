@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Tambah Supplier
                </div>

                <div class="card-body">
                    <form action="/supplier" method="POST">
                        @csrf
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" class="form-control" name="name" required/>
                        </div>
                        <div class="form-group">
                            <label>Nomor HP</label>
                            <input type="text" class="form-control" name="phone" required/>
                        </div>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <a href="/supplier" class="btn btn-default" style="border: 1px solid black">Batal</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
