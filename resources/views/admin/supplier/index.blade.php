@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Data Supplier
                    <a style="float:right; font-weight:bold" href="/supplier/create" class="btn btn-primary">+</a>
                </div>

                <div class="card-body">
                    <div style="overflow: scroll; width: 100%">
                    <table class="table table-hover table-stripped" >
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>HP</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $index => $item)
                            <tr>
                                <td>{{ $index+1 }}</td>
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->phone }}</td>
                                <td>
                                    <a href="/supplier/edit/{{$item->id}}" class="btn btn-primary"> E</a>
                                    <a href="/supplier/delete/{{$item->id}}" class="btn btn-danger"> D</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
