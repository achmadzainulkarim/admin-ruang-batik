@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Edit Supplier
                </div>

                <div class="card-body">
                    <form action="/supplier/{{$data->id}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" class="form-control" name="name" required value="{{$data->name}}"/>
                        </div>
                        <div class="form-group">
                            <label>Nomor HP</label>
                            <input type="text" class="form-control" name="phone" required value="{{$data->phone}}"/>
                        </div>
                        <button type="submit" class="btn btn-primary">Edit</button>
                        <a href="/supplier" class="btn btn-danger">Batal</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
