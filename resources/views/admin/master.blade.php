@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Master Data</div>
                <div class="card-body">
                    <a href="/supplier" class="btn btn-primary" style="margin-bottom: 10px;">Supplier</a>
                    <br><a href="/product" class="btn btn-primary" style="margin-bottom: 10px;">Produk</a>
                    <br><a href="/courier" class="btn btn-primary" style="margin-bottom: 10px;">Kurir</a>
                    <br><a href="/status" class="btn btn-primary" style="margin-bottom: 10px;">Status</a>
                    <br><a href="/size" class="btn btn-primary" style="margin-bottom: 10px;">Ukuran</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
