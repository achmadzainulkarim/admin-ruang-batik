@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Edit Kurir
                </div>

                <div class="card-body">
                    <form action="/courier/{{$data->id}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" class="form-control" name="name" required value="{{$data->name}}"/>
                        </div>
                        <button type="submit" class="btn btn-primary">Edit</button>
                        <a href="/courier" class="btn btn-danger">Batal</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
