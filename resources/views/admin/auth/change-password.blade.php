@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Ganti Password
                </div>

                <div class="card-body">
                    <h5 style="color: red">{{ $message }}</h5>
                    @if($is_form)
                    <form action="/change-password" method="POST">
                        @csrf
                        <div class="form-group">
                            <label>Password Lama</label>
                            <input type="password" class="form-control" name="old_pass" required/>
                        </div>
                        <div class="form-group">
                            <label>Password Baru</label>
                            <input type="password" class="form-control" name="new_pass" required/>
                        </div>
                        <div class="form-group">
                            <label>Konfirmasi Password Baru</label>
                            <input type="password" class="form-control" name="new_pass2" required/>
                        </div>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
