@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Tambah Pengeluaran
                </div>

                <div class="card-body">
                    <form action="/sale" method="POST">
                        @csrf
                        <input type="hidden" name="user_id" value="{{$user_id}}"/>
                        <div class="form-group">
                            <label>Tanggal</label>
                            <input type="date" class="form-control" name="date" required/>
                        </div>
                        <div class="form-group">
                            <label>Customer</label>
                            <input type="text" name="customer" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <label>Produk</label>
                            <select class="form-control" name="product_id">
                                @foreach ($product as $pi)
                                    <option value="{{ $pi->id }}">{{ $pi->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Ukuran</label>
                            <select class="form-control" name="size_id">
                                @foreach ($size as $si)
                                    <option value="{{ $si->id }}">{{ $si->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Supplier</label>
                            <select class="form-control" name="supplier_id">
                                @foreach ($supplier as $si)
                                    <option value="{{ $si->id }}">{{ $si->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Beli</label>
                            <input type="number" name="buy" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <label>Jual</label>
                            <input type="number" name="sell" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <label>Jumlah</label>
                            <input type="number" name="quantity" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <label>Diskon</label>
                            <input type="number" name="discount" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <label>Kurir</label>
                            <select class="form-control" name="courier_id">
                                @foreach ($courier as $si)
                                    <option value="{{ $si->id }}">{{ $si->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Ongkos Kirim</label>
                            <input type="number" name="delivery" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <label>Status</label>
                            <select class="form-control" name="status_id">
                                @foreach ($status as $si)
                                    <option value="{{ $si->id }}">{{ $si->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>COD?</label>
                            <br>
                            <input type="number" name="cod" class="form-control" placeholder="isi 1 untuk COD, dan 0 untuk dropship" required/>
                        </div>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <a href="/sale" class="btn btn-default" style="border: 1px solid black">Batal</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
