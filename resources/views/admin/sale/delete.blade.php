@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Edit Ukuran
                </div>

                <div class="card-body">
                    <form action="/sale/{{$data->id}}/delete" method="post">
                        @csrf
                        Apakah anda yakin untuk menghapus data dari {{ $data->customer }} ?
                        <br>
                        <button type="submit" class="btn btn-danger">Hapus</button>
                        <a href="/sale" class="btn btn-primary">Batal</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
