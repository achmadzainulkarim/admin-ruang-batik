@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Edit Pengeluaran
                </div>

                <div class="card-body">
                    <form action="/sale/{{$data->id}}" method="POST">
                        @csrf
                        <div class="form-group">
                                <label>Tanggal</label>
                            <input type="date" class="form-control" name="date" required value="{{$data->date}}" required/>
                            </div>
                            <div class="form-group">
                                <label>Customer</label>
                                <input type="text" name="customer" class="form-control" value="{{$data->customer}}" required/>
                            </div>
                            <div class="form-group">
                                <label>Produk</label>
                                <select class="form-control" name="product_id">
                                    @foreach ($product as $pi)
                                        <option value="{{ $pi->id }}" @if($pi->id == $data->product_id) {{ "selected" }} @endif>{{ $pi->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Ukuran</label>
                                <select class="form-control" name="size_id">
                                    @foreach ($size as $si)
                                        <option value="{{ $si->id }}" @if($si->id == $data->size_id) {{ "selected" }} @endif>{{ $si->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Supplier</label>
                                <select class="form-control" name="supplier_id">
                                    @foreach ($supplier as $si)
                                        <option value="{{ $si->id }}" @if($si->id == $data->supplier_id) {{ "selected" }} @endif>
                                        {{ $si->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Beli</label>
                                <input type="number" name="buy" class="form-control" value="{{$data->buy}}" required/>
                            </div>
                            <div class="form-group">
                                <label>Jual</label>
                                <input type="number" name="sell" class="form-control" value="{{$data->sell}}" required/>
                            </div>
                            <div class="form-group">
                                <label>Jumlah</label>
                                <input type="number" name="quantity" class="form-control" value="{{$data->quantity}}" required/>
                            </div>
                            <div class="form-group">
                                <label>Diskon</label>
                                <input type="number" name="discount" class="form-control" value="{{$data->discount}}" required/>
                            </div>
                            <div class="form-group">
                                <label>Kurir</label>
                                <select class="form-control" name="courier_id">
                                    @foreach ($courier as $si)
                                        <option value="{{ $si->id }}" @if($si->id == $data->courier_id) {{ "selected" }} @endif>{{ $si->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Ongkos Kirim</label>
                                <input type="number" name="delivery" class="form-control" value="{{$data->delivery}}" required/>
                            </div>
                            <div class="form-group">
                                <label>Status</label>
                                <select class="form-control" name="status_id">
                                    @foreach ($status as $si)
                                        <option value="{{ $si->id }}" @if($si->id == $data->status_id) {{ "selected" }} @endif>{{ $si->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>COD?</label>
                                <br>
                                <input type="number" name="cod" class="form-control" placeholder="isi 1 untuk COD, dan 0 untuk dropship" value="{{$data->cod}}" required/>
                            </div>
                        <button type="submit" class="btn btn-primary">Edit</button>
                        <a href="/sale" class="btn btn-danger">Batal</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
