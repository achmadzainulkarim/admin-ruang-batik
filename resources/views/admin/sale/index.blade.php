@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Data Penjualan
                    <a style="float:right; font-weight:bold" href="/sale/create" class="btn btn-primary">+</a>
                </div>

                <div class="card-body">
                    <form action="" method="GET">
                        <label>Month:</label>
                        <input type="month" name="month" value="{{$month}}"/>
                        <button type="submit">Filter</button>
                    </form>
                    <div style="overflow: scroll; width: 100%">
                    <table class="table table-hover table-stripped" >
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tangal</th>
                                <th>Customer</th>
                                <th>Produk</th>
                                <th>Uk</th>
                                <th>Supplier</th>
                                <th>Beli</th>
                                <th>Jual</th>
                                <th>Q</th>
                                <th>Diskon</th>
                                <th>Earn</th>
                                <th>Kurir</th>
                                <th>Ongkir</th>
                                <th>COD</th>
                                <th>Status</th>
                                <th>Admin</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $index => $item)
                            <tr>
                                <td>{{ $index+1 }}</td>
                                <td>{{ $item->date }}</td>
                                <td>{{ $item->customer }}</td>
                                <td>{{ $item->product->name }}</td>
                                <td>{{ $item->size->name }}</td>
                                <td>{{ $item->supplier->name }}</td>
                                <td style="text-align:right">{{ number_format($item->buy) }}</td>
                                <td style="text-align:right">{{ number_format($item->sell) }}</td>
                                <td>{{ $item->quantity }}</td>
                                <td style="text-align:right">{{ number_format($item->discount) }}</td>
                                <td style="text-align:right">{{ number_format(($item->sell - $item->buy) * $item->quantity - $item->discount) }}</td>
                                <td>{{ $item->courier->name }}</td>
                                <td style="text-align:right">{{ number_format($item->delivery) }}</td>
                                <td>{{ $item->cod }}</td>
                                <td>{{ $item->status->name }}</td>
                                <td>{{ $item->user->name }}</td>
                                <td>
                                    <a href="/sale/edit/{{$item->id}}" class="btn btn-primary"> E</a>
                                    <a href="/sale/delete/{{$item->id}}" class="btn btn-danger"> D</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
