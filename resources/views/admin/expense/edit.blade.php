@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Edit Pengeluaran
                </div>

                <div class="card-body">
                    <form action="/expense/{{$data->id}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label>Tanggal</label>
                            <input type="date" class="form-control" name="date" required value="{{$data->date}}"/>
                        </div>
                        <div class="form-group">
                            <label>Provider</label>
                            <input type="text" class="form-control" name="provider" required value="{{$data->provider}}"/>
                        </div>
                        <div class="form-group">
                            <label>Item</label>
                            <input type="text" class="form-control" name="item" required value="{{$data->item}}"/>
                        </div>
                        <div class="form-group">
                            <label>Jumlah</label>
                            <input type="number" class="form-control" name="quantity" required value="{{$data->quantity}}"/>
                        </div>
                        <div class="form-group">
                            <label>Harga</label>
                            <input type="number" class="form-control" name="price" required value="{{$data->price}}"/>
                        </div>
                        <button type="submit" class="btn btn-primary">Edit</button>
                        <a href="/expense" class="btn btn-danger">Batal</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
