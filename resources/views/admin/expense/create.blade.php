@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Tambah Pengeluaran
                </div>

                <div class="card-body">
                    <form action="/expense" method="POST">
                        @csrf
                        <div class="form-group">
                            <label>Tanggal</label>
                            <input type="date" class="form-control" name="date" required/>
                        </div>
                        <div class="form-group">
                            <label>Provider</label>
                            <input type="text" class="form-control" name="provider" required/>
                        </div>
                        <div class="form-group">
                            <label>Item</label>
                            <input type="text" class="form-control" name="item" required/>
                        </div>
                        <div class="form-group">
                            <label>Jumlah</label>
                            <input type="number" class="form-control" name="quantity" required/>
                        </div>
                        <div class="form-group">
                            <label>Harga</label>
                            <input type="number" class="form-control" name="price" required/>
                        </div>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <a href="/expense" class="btn btn-default" style="border: 1px solid black">Batal</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
