@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Data Pengeluaran
                    <a style="float:right; font-weight:bold" href="/expense/create" class="btn btn-primary">+</a>
                </div>

                <div class="card-body">
                    <div style="overflow: scroll; width: 100%">
                    <table class="table table-hover table-stripped" >
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tangal</th>
                                <th>Provider</th>
                                <th>Item</th>
                                <th>Jumlah</th>
                                <th>Harga</th>
                                <th>Total</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $index => $item)
                            <tr>
                                <td>{{ $index+1 }}</td>
                                <td>{{ $item->date }}</td>
                                <td>{{ $item->provider }}</td>
                                <td>{{ $item->item }}</td>
                                <td>{{ $item->quantity }}</td>
                                <td style="text-align: right">{{ number_format($item->price) }}</td>
                                <td style="text-align: right">{{ number_format($item->quantity * $item->price) }}</td>
                                <td>
                                    <a href="/expense/edit/{{$item->id}}" class="btn btn-primary"> E</a>
                                    <a href="/expense/delete/{{$item->id}}" class="btn btn-danger"> D</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
