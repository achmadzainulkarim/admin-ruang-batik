@extends('layouts.app')

@section('script')
<script>
        window.onload = function () {
        var monthly = <?php echo json_encode($testJson) ?>;
        let dataNew = [];
        monthly.map( item => {
            dataNew.push({x:parseInt(item.x), y:parseInt(item.y)});
        })

        var courier = <?php echo json_encode($courier) ?>;
        var totalTrans = <?php echo json_encode($total->transaksi) ?>;
        
        dataCourier = [];
        courier.map( item => {
            dataCourier.push({y:item.total, label:item.name});
        })

        var chart = new CanvasJS.Chart("chartContainer", {
            animationEnabled: true,
            zoomEnabled: true,
            theme: "light",
            title:{
                text: "Total Penjualan Tiap Bulan"
            },
            axisX:{
                title: "Bulan",
                valueFormatString: "####",
                interval: 1
            },
            axisY:{
                title: "Total Pcs",
                titleFontColor: "#6D78AD",
                lineColor: "#6D78AD",
                gridThickness: 0,
                lineThickness: 1
            },
            legend:{
                verticalAlign: "top",
                fontSize: 16,
                dockInsidePlotArea: true
            },
            data: [{
                type: "line",
                showInLegend: true,
                name: "",
                dataPoints: dataNew
                
            }]
        });
        chart.render();

        var pieChart = new CanvasJS.Chart("pieChartContainer", {
            animationEnabled: true,
            title: {
                text: "Lifetime Courier"
            },
            data: [{
                type: "pie",
                startAngle: 240,
                indexLabel: "{label} {y}",
                dataPoints: dataCourier
            }]
        });
        pieChart.render();
        
        }
        </script>
<script src="/js/canvasjs/canvasjs.min.js"></script>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-3 topBar">
                            <div class="cardState">
                                <div class="cardStateLeft" style="background: #dd4b39">
                                    <span class="fa fa-user"></span>
                                </div>
                                <div class="cardStateRight">
                                    <label>Total Customer</label><br>
                                    <label class="titleTopBar">{{ $total->cust }}</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 topBar">
                            <div class="cardState">
                                <div class="cardStateLeft" style="background: #3b5998">
                                    <span class="fa fa-cart-arrow-down"></span>
                                </div>
                                <div class="cardStateRight">
                                    <label>Total Transaksi</label><br>
                                    <label class="titleTopBar">{{ $total->transaksi }}</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 topBar">
                            <div class="cardState">
                                <div class="cardStateLeft" style="background: #ff6600">
                                    <span class="fa fa-briefcase"></span>
                                </div>
                                <div class="cardStateRight">
                                    <label>Rata-Rata Omzet</label><br>
                                    <label class="titleTopBar">{{ number_format($omzet) }}</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 topBar">
                            <div class="cardState">
                                <div class="cardStateLeft" style="background: #2c9f45">
                                    <span class="fa fa-shopping-cart"></span>
                                </div>
                                <div class="cardStateRight">
                                    <label>Rata-Rata Penjualan</label><br>
                                    <label class="titleTopBar"> {{ $sales }} </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12" style="margin-bottom: 10px;">
                            <div id="chartContainer" style="height: 300px; width: 100%; z-index: 10"></div>
                        </div>
                        <div class="col-md-4"><br><br>
                            <h4>Monthly Sales</h4>
                            @foreach ($monthly as $index => $item)
                            <div class="card">
                                <div class="card-header">{{ $item->new_date }}</div>
                                <div class="card-body">
                                    <label style="font-weight: bold">Omzet :</label><br>
                                    <span>Rp. {{ number_format($item->omzet) }}</span><br><br>
                                    <label style="font-weight: bold">Total Pcs :</label><br>
                                    <span>{{ $item->pcs }} Pcs.</span><br><br>
                                    <label style="font-weight: bold">Total Earnings :</label><br>
                                    <span>Rp. {{ number_format($item->earning) }}</span><br><br>
                                    <label style="font-weight: bold">Total Expense :</label><br>
                                    <span>Rp. {{ number_format($monthly_expense[$index]->total) }}</span><br><br>
                                    <label style="font-weight: bold">Total Profit :</label><br>
                                    <span>Rp. {{ number_format($item->earning - $monthly_expense[$index]->total) }}</span><br><br>
                                </div>
                            </div>
                            <br>
                            @endforeach
                        </div>
                        <div class="col-md-4">
                                <br><br>
                            <h4>Lifetime</h4>
                            <div class="card">
                                <div class="card-header">Lifetime</div>
                                <div class="card-body">
                                    <label style="font-weight: bold">Omzet :</label><br>
                                    <span>Rp. {{ number_format($lifetime->omzet) }}</span><br><br>
                                    <label style="font-weight: bold">Total Pcs :</label><br>
                                    <span>{{ $lifetime->total }} Pcs.</span><br><br>
                                    <label style="font-weight: bold">Total Earnings :</label><br>
                                    <span>Rp. {{ number_format($lifetime->earning) }}</span><br><br>
                                    <label style="font-weight: bold">Total Expense :</label><br>
                                    <span>Rp. {{ number_format($expense_lifetime->total) }}</span><br><br>
                                    <label style="font-weight: bold">Total Profit :</label><br>
                                    <span>Rp. {{ number_format($lifetime->earning - $expense_lifetime->total) }}</span><br><br>
                                </div>
                            </div>
                            <br>
                            <h4>Supplier Counter</h4>
                            <div class="card">
                                <div class="card-header">Lifetime</div>
                                <div class="card-body">
                                    <div style="overflow: scroll; width: 100%">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Total</th>
                                                    <th>Total Pcs</th>
                                                    <th>Omzet</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach( $supplier as $sItem)
                                                <tr>
                                                    <td style="text-transform: capitalize">{{ $sItem->name }}</td>
                                                    <td>{{ $sItem->transaksi }}</td>
                                                    <td>{{ $sItem->qty }}</td>
                                                    <td>{{ number_format($sItem->omzet) }}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <h4>Customer Repeat Order</h4>
                            <div class="card">
                                <div class="card-header">Lifetime</div>
                                <div class="card-body">
                                    <div style="overflow: scroll; width: 100%">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Repeat Order (x)</th>
                                                    <th>Total Pcs</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach( $repeat as $sItem)
                                                <tr>
                                                    <td style="text-transform: capitalize">{{ $sItem->customer }}</td>
                                                    <td>{{ $sItem->repeat_order - 1  }}</td>
                                                    <td>{{ $sItem->total }}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <h4>Admin Fee</h4>
                            <div class="card">
                                <div class="card-header">Monthly</div>
                                <div class="card-body">
                                    <div style="overflow: scroll; width: 100%">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Month</th>
                                                    <th>Total</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach( $admin as $sItem)
                                                <tr>
                                                    <td style="text-transform: capitalize">{{ $sItem->name }}</td>
                                                    <td>{{ $sItem->month  }}</td>
                                                    <td>{{ number_format($sItem->total_fee) }}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <br><br>
                            <h4>Courier</h4>
                            <div id="pieChartContainer" style="height: 370px; margin: 0px auto;"></div>
                            <br>
                            <div class="card">
                                <div class="card-header">Status</div>
                                <div class="card-body" >
                                    <div style="overflow: scroll; width: 100%">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Total</th>
                                                    <th>Total Pcs</th>
                                                    <th>Omzet</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach( $status as $sItem)
                                                <tr>
                                                    <td style="text-transform: capitalize">{{ $sItem->name }}</td>
                                                    <td>{{ $sItem->total }}</td>
                                                    <td>{{ $sItem->pcs }}</td>
                                                    <td>{{ number_format($sItem->omzet) }}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="card">
                                <div class="card-header">Customer (5 Most)</div>
                                <div class="card-body" >
                                    <div style="overflow: scroll; width: 100%">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Total Transaction (pcs)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach( $customer as $sC)
                                                <tr>
                                                    <td style="text-transform: capitalize">{{ $sC->customer }}</td>
                                                    <td>{{ $sC->q }} pcs</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="card">
                                <div class="card-header">Stock (COD Failed)</div>
                                <div class="card-body" >
                                    <div style="overflow: scroll; width: 100%">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Month</th>
                                                    <th>Pcs</th>
                                                    <th>Total Price</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach( $stock as $s)
                                                <tr>
                                                    <td style="text-transform: capitalize">{{ $s->month }}</td>
                                                    <td>{{ $s->qty }} pcs</td>
                                                    <td>Rp. {{ number_format($s->omzet) }},-</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
