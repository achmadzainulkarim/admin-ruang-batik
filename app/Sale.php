<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $fillable = [
        'date', 'customer', 'product_id', 'size_id', 'supplier_id', 'buy', 'sell', 'quantity', 
        'discount', 'courier_id', 'cod', 'user_id', 'delivery', 'status_id'
    ];

    public function product(){
        return $this->belongsTo('App\Product');
    }
    public function size(){
        return $this->belongsTo('App\Size');
    }
    public function supplier(){
        return $this->belongsTo('App\Supplier');
    }
    public function courier(){
        return $this->belongsTo('App\Courier');
    }
    public function user(){
        return $this->belongsTo('App\User');
    }
    public function status(){
        return $this->belongsTo('App\Status');
    }
}
