<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Supplier;
use App\Product;
use App\Courier;
use App\Status;
use App\Size;
use App\Expense;
use App\Sale;
use DB;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $d = date('Y-m');
        $lifetime = DB::select("select sum(sell*quantity) as omzet, sum(quantity) as total, sum(((sell - buy) * quantity) - discount) as earning, sum(discount) as discount, sum(delivery) as delivery from sales left join statuses s on s.id = status_id where s.name != 'stock'");

        $expenseLifetime = DB::select("SELECT sum(quantity * price) as total FROM `expenses`");

        $monthly = DB::select("SELECT substr(date, 1, 7) as new_date, sum(quantity) as pcs, sum(sell*quantity) as omzet, sum(((sell - buy) * quantity) - discount) as earning, sum(discount) as discount, sum(delivery) as delivery FROM `sales` left join statuses s on s.id = status_id where s.name != 'stock' GROUP by new_date order by new_date DESC");

        $monthly_ex = DB::select("SELECT substr(date, 1, 7) as new_date, sum(price * quantity) as total FROM `expenses` GROUP by new_date order by new_date DESC");
        $total = DB::select("SELECT count(id) as transaksi, count(DISTINCT customer) as cust from sales");

        $courier = DB::select("SELECT couriers.name, count(sales.id) as total from sales LEFt join couriers on sales.courier_id = couriers.id  GROUP by couriers.name");

        $status = DB::select("SELECT statuses.name, count(sales.id) as total, sum(sales.quantity) as pcs, sum(((sell - buy) * quantity) - discount) as earning, sum(sell*quantity) as omzet from sales LEFt join statuses on sales.status_id = statuses.id GROUP by statuses.name");

        $customer = DB::select("select customer, sum(quantity) as q from sales group by customer order by q desc limit 5");

        $jsonData = [];
        $omzet = (count($monthly) == 0) ? 0 : $lifetime[0]->omzet / count($monthly);
        $sales = (count($monthly) == 0) ? 0 : round($lifetime[0]->total / count($monthly));

        $supplier = DB::select("SELECT suppliers.name as name, count(sales.id) as transaksi, sum(sales.quantity) as qty, sum(sales.quantity * sales.sell) as omzet FROM `sales` left join suppliers on suppliers.id = sales.supplier_id group by name order by qty desc, transaksi desc");

        $repeat_order = DB::select("select customer, count(q) as repeat_order, sum(q) as total from (select date, customer, sum(quantity) as q from sales left join statuses s on s.id = status_id where s.name != 'stock' group by date, customer) as p GROUP by customer order by repeat_order desc, total desc limit 5");

        $admin = DB::select("select month,name, sum(fee * quantity) as total_fee from (SELECT u.name, quantity, substr(s.date, 1, 7) as month, case when (s.sell - s.buy) >= 30000 then 10000 else 5000 end as fee FROM `sales` s inner join users u on u.id = s.user_id left join statuses st on st.id = status_id where st.name != 'stock') as f group by month, name order by name asc, month desc");

        $stock = DB::select("select sum(quantity) as qty, substr(date, 1, 7) as month, sum(sell*quantity) as omzet from sales where status_id = 7 group by month");

        foreach($monthly as $item){
            $jsonData[] = [
                "x" => substr($item->new_date, 5,2),
                "y" => $item->pcs
            ];
        }
        return view('home', [
            'lifetime' => $lifetime[0],
            'expense_lifetime' => $expenseLifetime[0],
            'monthly' => $monthly,
            'monthly_expense' => $monthly_ex,
            'testJson' => $jsonData,
            'total' => $total[0],
            'omzet' => $omzet,
            'sales' => $sales,
            'courier' => $courier,
            'status' => $status,
            'customer' => $customer,
            'supplier' => $supplier,
            'repeat' => $repeat_order,
            'admin' => $admin,
            'stock' => @$stock,
        ]);
    }

    public function master()
    {
        return view('admin.master');
    }

    public function sales()
    {
        return view('admin.sales.sales');
    }

    public function expenses(){
        return view('admin.expenses.expenses');
    }

    // supplier
    public function getSupplier(){
        $data = Supplier::all();
        return view('admin.supplier.index', ['data' => $data]);
    }

    public function createViewSupplier(){
        return view('admin.supplier.create');
    }

    public function editViewSupplier($id){
        $data = Supplier::find($id);
        return view('admin.supplier.edit', ['data' => $data]);
    }

    public function deleteViewSupplier($id){
        $data = Supplier::find($id);
        return view('admin.supplier.delete', ['data' => $data]);
    }

    public function createSupplier(Request $request){
        $data = Supplier::create([
            'name' => @$request->name,
            'phone' => @$request->phone,
        ]);
        return redirect('/supplier');
    }

    public function updateSupplier($id, Request $request){
        $data = Supplier::find($id);
        $data->update([
            'name' => @$request->name,
            'phone' => @$request->phone,
        ]);
        return redirect('/supplier');
    }

    public function deleteSupplier($id){  
        $data = Supplier::find($id);
        $data->delete();
        return redirect('/supplier');
    }


    // Product
    public function getProduct(){
        $data = Product::all();
        return view('admin.product.index', ['data' => $data]);
    }

    public function createViewProduct(){
        return view('admin.product.create');
    }

    public function editViewProduct($id){
        $data = Product::find($id);
        return view('admin.product.edit', ['data' => $data]);
    }

    public function deleteViewProduct($id){
        $data = Product::find($id);
        return view('admin.product.delete', ['data' => $data]);
    }

    public function createProduct(Request $request){
        $data = Product::create([
            'name' => @$request->name,
        ]);
        return redirect('/product');
    }

    public function updateProduct($id, Request $request){
        $data = Product::find($id);
        $data->update([
            'name' => @$request->name,
        ]);
        return redirect('/product');
    }

    public function deleteProduct($id){  
        $data = Product::find($id);
        $data->delete();
        return redirect('/product');
    }

     // Courier
     public function getCourier(){
        $data = Courier::all();
        return view('admin.courier.index', ['data' => $data]);
    }

    public function createViewCourier(){
        return view('admin.courier.create');
    }

    public function editViewCourier($id){
        $data = Courier::find($id);
        return view('admin.courier.edit', ['data' => $data]);
    }

    public function deleteViewCourier($id){
        $data = Courier::find($id);
        return view('admin.courier.delete', ['data' => $data]);
    }

    public function createCourier(Request $request){
        $data = Courier::create([
            'name' => @$request->name,
        ]);
        return redirect('/courier');
    }

    public function updateCourier($id, Request $request){
        $data = Courier::find($id);
        $data->update([
            'name' => @$request->name,
        ]);
        return redirect('/courier');
    }

    public function deleteCourier($id){  
        $data = Courier::find($id);
        $data->delete();
        return redirect('/courier');
    }

    // Status
    public function getStatus(){
        $data = Status::all();
        return view('admin.status.index', ['data' => $data]);
    }

    public function createViewStatus(){
        return view('admin.status.create');
    }

    public function editViewStatus($id){
        $data = Status::find($id);
        return view('admin.status.edit', ['data' => $data]);
    }

    public function deleteViewStatus($id){
        $data = Status::find($id);
        return view('admin.status.delete', ['data' => $data]);
    }

    public function createStatus(Request $request){
        $data = Status::create([
            'name' => @$request->name,
        ]);
        return redirect('/status');
    }

    public function updateStatus($id, Request $request){
        $data = Status::find($id);
        $data->update([
            'name' => @$request->name,
        ]);
        return redirect('/status');
    }

    public function deleteStatus($id){  
        $data = Status::find($id);
        $data->delete();
        return redirect('/status');
    }

    // Size
    public function getSize(){
        $data = Size::all();
        return view('admin.size.index', ['data' => $data]);
    }

    public function createViewSize(){
        return view('admin.size.create');
    }

    public function editViewSize($id){
        $data = Size::find($id);
        return view('admin.size.edit', ['data' => $data]);
    }

    public function deleteViewSize($id){
        $data = Size::find($id);
        return view('admin.size.delete', ['data' => $data]);
    }

    public function createSize(Request $request){
        $data = Size::create([
            'name' => @$request->name,
        ]);
        return redirect('/size');
    }

    public function updateSize($id, Request $request){
        $data = Size::find($id);
        $data->update([
            'name' => @$request->name,
        ]);
        return redirect('/size');
    }

    public function deleteSize($id){  
        $data = Size::find($id);
        $data->delete();
        return redirect('/size');
    }

    // Expense
    public function getExpense(){
        $data = Expense::orderBy("date", "desc")->get();
        return view('admin.expense.index', ['data' => $data]);
    }

    public function createViewExpense(){
        return view('admin.expense.create');
    }

    public function editViewExpense($id){
        $data = Expense::find($id);
        return view('admin.expense.edit', ['data' => $data]);
    }

    public function deleteViewExpense($id){
        $data = Expense::find($id);
        return view('admin.expense.delete', ['data' => $data]);
    }

    public function createExpense(Request $request){
        $data = Expense::create($request->all());
        return redirect('/expense');
    }

    public function updateExpense($id, Request $request){
        $data = Expense::find($id);
        $data->update($request->all());
        return redirect('/expense');
    }

    public function deleteExpense($id){  
        $data = Expense::find($id);
        $data->delete();
        return redirect('/expense');
    }
    

    // Sale
    public function getSale(Request $request){
        $d = (@$request->month == null) ? date('Y-m') : $request->month;
        $data = Sale::where('date', 'like', "{$d}%")->orderBy('date')->get();
        return view('admin.sale.index', [
            'data' => $data,
            'month' => $d,
        ]);
    }

    public function createViewSale(){
        $product = Product::all();
        $size = Size::all();
        $supp = Supplier::all();
        $courier = Courier::all();
        $status = Status::all();
        return view('admin.sale.create', [
            'product' => $product,
            'size' => $size,
            'supplier' => $supp,
            'courier' => $courier,
            'status' => $status,
            'user_id' => Auth::user()->id,
        ]);
    }

    public function editViewSale($id){
        $product = Product::all();
        $size = Size::all();
        $supp = Supplier::all();
        $courier = Courier::all();
        $data = Sale::find($id);
        $status = Status::all();
        return view('admin.sale.edit', [
            'data' => $data,
            'product' => $product,
            'size' => $size,
            'supplier' => $supp,
            'status' => $status,
            'courier' => $courier,
        ]);
    }

    public function deleteViewSale($id){
        $data = Sale::find($id);
        return view('admin.sale.delete', ['data' => $data]);
    }

    public function createSale(Request $request){
        $data = Sale::create($request->all());
        return redirect('/sale');
    }

    public function updateSale($id, Request $request){
        $data = Sale::find($id);
        $data->update($request->all());
        return redirect('/sale');
    }

    public function deleteSale($id){  
        $data = Sale::find($id);
        $data->delete();
        return redirect('/sale');
    }
}
