<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use App\User;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function changePassword(){
        return view('admin.auth.change-password', [
            'is_form' => true,
            'message' => "",
        ]);
    }

    public function postChangePassword(Request $request){
        $message = "";
        $isTrue = Hash::check($request->old_pass, auth()->user()->password);
        if(!$isTrue){
            $message = "Password lama tidak sesuai";
            $form = true;
        }
        else{
            if($request->new_pass != $request->new_pass2){
                $message = "Konfirmasi password baru tidak sesuai";
                $form = true;
            }
            else{
                $user = User::find(auth()->user()->id);
                $user->update([
                    'password' => Hash::make($request->new_pass)
                ]);
                $message = "Password berhasil dirubah, silahkan login kembali";
                $form = false;
            }
        }
        return view('admin.auth.change-password', [
            'is_form' => $form,
            'message' => $message,
        ]);
    }
}