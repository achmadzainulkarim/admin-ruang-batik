<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Route::get('/error', function () { return view('error'); });

Auth::routes();

Route::group(['middleware' => 'admin'], function(){
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/master', 'HomeController@master')->name('home');

    // supplier
    Route::get('/supplier', 'HomeController@getSupplier')->name('get_supplier');
    Route::get('/supplier/create', 'HomeController@createViewSupplier')->name('create_supplier');
    Route::get('/supplier/edit/{id}', 'HomeController@editViewSupplier')->name('edit_supplier');
    Route::get('/supplier/delete/{id}', 'HomeController@deleteViewSupplier')->name('delete_supplier');
    Route::post('/supplier', 'HomeController@createSupplier')->name('post_create_supplier');
    Route::post('/supplier/{id}', 'HomeController@updateSupplier')->name('post_edit_supplier');
    Route::post('/supplier/{id}/delete', 'HomeController@deleteSupplier')->name('post_delete_supplier');

    // product
    Route::get('/product', 'HomeController@getproduct')->name('get_product');
    Route::get('/product/create', 'HomeController@createViewproduct')->name('create_product');
    Route::get('/product/edit/{id}', 'HomeController@editViewproduct')->name('edit_product');
    Route::get('/product/delete/{id}', 'HomeController@deleteViewproduct')->name('delete_product');
    Route::post('/product', 'HomeController@createproduct')->name('post_create_product');
    Route::post('/product/{id}', 'HomeController@updateproduct')->name('post_edit_product');
    Route::post('/product/{id}/delete', 'HomeController@deleteproduct')->name('post_delete_product');

    // courier
    Route::get('/courier', 'HomeController@getcourier')->name('get_courier');
    Route::get('/courier/create', 'HomeController@createViewcourier')->name('create_courier');
    Route::get('/courier/edit/{id}', 'HomeController@editViewcourier')->name('edit_courier');
    Route::get('/courier/delete/{id}', 'HomeController@deleteViewcourier')->name('delete_courier');
    Route::post('/courier', 'HomeController@createcourier')->name('post_create_courier');
    Route::post('/courier/{id}', 'HomeController@updatecourier')->name('post_edit_courier');
    Route::post('/courier/{id}/delete', 'HomeController@deletecourier')->name('post_delete_courier');

    // status
    Route::get('/status', 'HomeController@getstatus')->name('get_status');
    Route::get('/status/create', 'HomeController@createViewstatus')->name('create_status');
    Route::get('/status/edit/{id}', 'HomeController@editViewstatus')->name('edit_status');
    Route::get('/status/delete/{id}', 'HomeController@deleteViewstatus')->name('delete_status');
    Route::post('/status', 'HomeController@createstatus')->name('post_create_status');
    Route::post('/status/{id}', 'HomeController@updatestatus')->name('post_edit_status');
    Route::post('/status/{id}/delete', 'HomeController@deletestatus')->name('post_delete_status');

    // size
    Route::get('/size', 'HomeController@getsize')->name('get_size');
    Route::get('/size/create', 'HomeController@createViewsize')->name('create_size');
    Route::get('/size/edit/{id}', 'HomeController@editViewsize')->name('edit_size');
    Route::get('/size/delete/{id}', 'HomeController@deleteViewsize')->name('delete_size');
    Route::post('/size', 'HomeController@createsize')->name('post_create_size');
    Route::post('/size/{id}', 'HomeController@updatesize')->name('post_edit_size');
    Route::post('/size/{id}/delete', 'HomeController@deletesize')->name('post_delete_size');

    // expense
    Route::get('/expense', 'HomeController@getexpense')->name('get_expense');
    Route::get('/expense/create', 'HomeController@createViewexpense')->name('create_expense');
    Route::get('/expense/edit/{id}', 'HomeController@editViewexpense')->name('edit_expense');
    Route::get('/expense/delete/{id}', 'HomeController@deleteViewexpense')->name('delete_expense');
    Route::post('/expense', 'HomeController@createexpense')->name('post_create_expense');
    Route::post('/expense/{id}', 'HomeController@updateexpense')->name('post_edit_expense');
    Route::post('/expense/{id}/delete', 'HomeController@deleteexpense')->name('post_delete_expense');

    // sale
    Route::get('/sale', 'HomeController@getsale')->name('get_sale');
    Route::get('/sale/create', 'HomeController@createViewsale')->name('create_sale');
    Route::get('/sale/edit/{id}', 'HomeController@editViewsale')->name('edit_sale');
    Route::get('/sale/delete/{id}', 'HomeController@deleteViewsale')->name('delete_sale');
    Route::post('/sale', 'HomeController@createsale')->name('post_create_sale');
    Route::post('/sale/{id}', 'HomeController@updatesale')->name('post_edit_sale');
    Route::post('/sale/{id}/delete', 'HomeController@deletesale')->name('post_delete_sale');

    // user
    Route::get('/change-password', 'AuthController@changePassword')->name('get_change_password');
    Route::post('/change-password', 'AuthController@postChangePassword')->name('post_change_password');
});