-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: localhost    Database: adminruangbatik
-- ------------------------------------------------------
-- Server version	5.7.24-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `couriers`
--

DROP TABLE IF EXISTS `couriers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `couriers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `couriers`
--

LOCK TABLES `couriers` WRITE;
/*!40000 ALTER TABLE `couriers` DISABLE KEYS */;
INSERT INTO `couriers` VALUES (1,'JNE','2019-10-29 01:18:36','2019-10-29 01:18:36'),(2,'Wahana','2019-10-29 01:18:42','2019-10-29 01:18:42'),(3,'POS','2019-10-29 01:18:47','2019-10-29 01:18:47'),(4,'J&T','2019-10-29 01:18:56','2019-10-29 01:18:56');
/*!40000 ALTER TABLE `couriers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `expenses`
--

DROP TABLE IF EXISTS `expenses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `expenses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `expenses`
--

LOCK TABLES `expenses` WRITE;
/*!40000 ALTER TABLE `expenses` DISABLE KEYS */;
INSERT INTO `expenses` VALUES (1,'2019-09-19','Agil','Desain Logo',1,200000,'2019-10-29 16:36:54','2019-10-29 16:36:54'),(2,'2019-09-30','Arisska Batik','Giveaway',1,120000,'2019-10-29 16:37:35','2019-10-29 16:37:35'),(3,'2019-09-19','Natanetwork','Domain',1,200000,'2019-10-29 16:38:20','2019-10-29 16:38:35'),(4,'2019-09-19','Natanetwork','Hosting',1,100000,'2019-10-29 16:39:04','2019-10-29 16:39:04'),(5,'2019-09-20','Instagram','Promote',3,10000,'2019-10-29 16:39:36','2019-10-29 16:39:36'),(6,'2019-10-02','Natanetwork','Hosting',1,100000,'2019-10-29 16:39:58','2019-10-29 16:39:58'),(7,'2019-10-10','Idwebtech','Followers',1,150000,'2019-10-29 16:40:27','2019-10-29 16:40:27'),(8,'2019-10-11','Arisska Batik','Ongkir Giveaway',1,17000,'2019-10-29 16:41:13','2019-10-29 16:41:13'),(9,'2019-10-13','Instagram','Promote',5,10000,'2019-10-29 16:41:43','2019-10-29 16:41:43'),(10,'2019-10-13','Permata','Admin Bank',1,6500,'2019-10-29 16:42:22','2019-10-29 16:42:22'),(11,'2019-10-24','-','Handphone Admin',1,775000,'2019-10-29 16:42:58','2019-10-29 16:42:58'),(12,'2019-10-25','Lucky Cell','Nomor Cantik M3',1,60000,'2019-10-29 16:46:29','2019-10-29 16:46:29'),(13,'2019-10-25','Lucky Cell','Nomor Cantik M3',1,40000,'2019-10-29 16:46:58','2019-10-29 16:46:58'),(14,'2019-10-25','Lucky Cell','Nomor Cantik Tsel',2,43000,'2019-10-29 16:47:23','2019-10-29 16:47:23'),(15,'2019-11-03','Natanetwork','Hosting',1,100000,'2019-11-04 11:34:15','2019-11-04 11:34:15'),(16,'2019-11-05','Instagram','Promote',5,10000,'2019-11-06 00:38:19','2019-11-06 00:38:19'),(17,'2019-11-09','Admin 1','Fee',1,80000,'2019-11-09 20:56:09','2019-11-09 20:56:09'),(19,'2019-11-21','Mas Randy','3 Desain Promosi',1,200000,'2019-11-23 11:16:27','2019-11-23 11:16:27'),(20,'2019-12-01','Smartfren','Paket Data',1,30000,'2019-12-02 00:56:48','2019-12-02 00:56:48'),(21,'2019-12-01','Smartfren','Paket Data',1,20000,'2019-12-02 00:57:11','2019-12-02 00:57:11'),(22,'2019-11-30','Admin 1','Fee',1,605000,'2019-12-06 01:56:15','2019-12-06 01:56:48');
/*!40000 ALTER TABLE `expenses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1),(4,'2019_10_28_123221_create_suppliers_table',1),(5,'2019_10_28_123240_create_products_table',1),(6,'2019_10_28_123248_create_couriers_table',1),(7,'2019_10_28_123254_create_statuses_table',1),(8,'2019_10_28_123300_create_sizes_table',1),(9,'2019_10_28_123311_create_expenses_table',1),(11,'2019_10_28_123318_create_sales_table',2),(12,'2019_10_29_123053_add_status_on_sales',3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'Hem','2019-10-29 01:16:54','2019-10-29 01:16:54'),(2,'Kemeja','2019-10-29 01:17:00','2019-10-29 01:17:00'),(3,'Blouse','2019-10-29 01:17:06','2019-10-29 01:17:06'),(4,'Tunik','2019-10-29 01:17:16','2019-10-29 01:17:16'),(5,'Longcardy','2019-10-29 01:17:27','2019-10-29 01:17:27'),(6,'Gamis','2019-10-29 01:17:34','2019-10-29 01:17:34'),(7,'Dress','2019-10-29 01:17:40','2019-10-29 01:17:40'),(8,'Bolero','2019-10-29 01:17:46','2019-10-29 01:17:46'),(9,'Bolero Bolak Balik','2019-10-29 01:17:56','2019-10-29 01:17:56'),(10,'Vest','2019-10-29 01:18:06','2019-10-29 01:18:06'),(11,'Kain','2019-12-26 00:44:07','2019-12-26 00:44:07');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales`
--

DROP TABLE IF EXISTS `sales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `customer` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` int(11) NOT NULL,
  `size_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `buy` int(11) NOT NULL,
  `sell` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `discount` int(11) NOT NULL DEFAULT '0',
  `courier_id` int(11) NOT NULL,
  `delivery` int(11) NOT NULL,
  `cod` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `status_id` int(11) DEFAULT '3',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sales_status_id_index` (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales`
--

LOCK TABLES `sales` WRITE;
/*!40000 ALTER TABLE `sales` DISABLE KEYS */;
INSERT INTO `sales` VALUES (1,'2019-09-19','Nalia',1,5,2,60000,80000,1,0,2,8000,1,1,3,'2019-10-29 08:53:20','2019-10-29 08:53:20'),(3,'2019-09-19','Nalia',1,5,2,80000,100000,1,20000,2,0,1,1,3,'2019-10-29 14:37:12','2019-10-29 14:37:12'),(4,'2019-09-25','Pamuji',2,3,5,60000,110000,1,5000,1,15000,0,1,3,'2019-10-29 14:38:59','2019-10-29 14:38:59'),(5,'2019-09-25','Dikpora Kediri',4,9,2,80000,90000,1,0,2,0,1,1,3,'2019-10-29 14:46:56','2019-10-29 14:46:56'),(6,'2019-09-25','Dikpora Kediri',2,4,2,65000,80000,1,0,2,0,1,1,3,'2019-10-29 14:48:10','2019-10-29 14:48:10'),(7,'2019-09-25','Dikpora Kediri',4,1,2,70000,80000,3,0,2,0,1,1,3,'2019-10-29 14:49:51','2019-10-29 14:49:51'),(8,'2019-09-25','Dikpora Kediri',2,5,2,65000,80000,2,0,2,0,1,1,3,'2019-10-29 14:51:58','2019-10-29 14:51:58'),(9,'2019-09-25','Dikpora Kediri',2,6,2,70000,80000,1,0,2,0,1,1,3,'2019-10-29 14:56:34','2019-10-29 14:56:34'),(10,'2019-09-25','Dikpora Kediri',1,6,2,90000,100000,2,0,2,0,1,1,3,'2019-10-29 14:58:08','2019-10-29 14:58:35'),(11,'2019-09-28','Bagus Pasuruan',1,4,2,60000,77000,1,0,2,8000,0,1,3,'2019-10-29 15:05:11','2019-10-29 15:05:11'),(12,'2019-09-29','Desiana',3,4,1,50000,90000,1,0,3,16000,0,1,3,'2019-10-29 15:06:15','2019-10-29 15:06:15'),(13,'2019-09-30','Dewi',3,7,1,50000,105000,1,0,2,0,0,1,3,'2019-10-29 15:07:16','2019-10-29 15:07:16'),(14,'2019-10-03','Dewi',1,5,2,60000,75000,1,0,2,6000,0,1,3,'2019-10-29 15:10:57','2019-10-29 15:10:57'),(15,'2019-10-04','Juliarto',1,4,3,30000,80000,1,0,2,6000,1,1,3,'2019-10-29 15:13:01','2019-10-29 15:13:01'),(16,'2019-10-04','Dewi',4,5,4,50000,90000,1,4000,1,14000,0,1,3,'2019-10-29 15:14:29','2019-10-29 15:14:29'),(17,'2019-10-05','Bagas',2,5,2,85000,120000,1,0,2,6000,0,1,3,'2019-10-29 15:15:36','2019-10-29 15:15:36'),(18,'2019-10-06','Eko Santoso',1,4,2,60000,85000,1,0,2,6000,0,1,3,'2019-10-29 15:20:24','2019-10-29 15:20:24'),(19,'2019-10-07','Agilwu',2,4,5,60000,105000,1,1000,2,6000,0,1,3,'2019-10-29 15:21:12','2019-10-29 15:21:12'),(20,'2019-10-07','Dewi',1,5,2,60000,75000,1,0,2,6000,0,1,3,'2019-10-29 15:21:59','2019-10-29 15:21:59'),(21,'2019-10-08','Dewi',4,5,4,50000,90000,1,0,1,14000,0,1,3,'2019-10-29 15:22:50','2019-10-29 15:22:50'),(22,'2019-10-09','Nehru',2,4,3,55000,105000,1,5000,1,14000,0,1,3,'2019-10-29 15:23:41','2019-10-29 16:18:11'),(23,'2019-10-13','Wiwit',2,4,3,50000,75000,4,6000,2,6000,1,1,3,'2019-10-29 15:24:55','2019-11-20 05:51:32'),(24,'2019-10-13','Wiwit',1,4,1,31000,45000,6,12000,2,12000,1,1,3,'2019-10-29 15:25:47','2019-11-20 05:51:53'),(25,'2019-10-13','Wiwit',4,4,1,50000,75000,1,0,2,0,1,1,3,'2019-10-29 15:26:58','2019-11-20 05:52:16'),(26,'2019-10-14','Aga',1,4,5,42000,75000,2,6000,2,6000,1,1,3,'2019-10-29 15:28:32','2019-10-29 15:28:32'),(27,'2019-10-15','Peni',1,4,1,31000,50000,2,10000,2,12000,0,1,3,'2019-10-29 15:30:08','2019-10-29 15:30:08'),(28,'2019-10-15','Peni',1,5,1,31000,50000,3,15000,2,0,0,1,3,'2019-10-29 15:30:55','2019-10-29 15:30:55'),(29,'2019-10-15','Dewi',4,4,4,50000,80000,1,0,1,14000,0,1,3,'2019-10-29 15:31:46','2019-10-29 15:31:46'),(30,'2019-10-18','Erni',2,6,2,90000,110000,1,0,2,0,1,1,3,'2019-10-29 15:32:43','2019-10-29 15:32:43'),(31,'2019-10-18','Erni',4,7,4,50000,90000,1,14000,1,14000,1,1,3,'2019-10-29 15:33:45','2019-10-29 15:33:45'),(32,'2019-10-21','Fanny',10,1,1,50000,90000,1,0,2,6000,0,1,3,'2019-10-29 15:34:45','2019-10-29 15:34:45'),(33,'2019-10-27','Peni',1,5,1,31000,50000,2,10000,2,12000,0,1,3,'2019-10-29 15:36:14','2019-11-05 16:42:28'),(34,'2019-10-27','Dewi',4,9,2,80000,95000,1,0,1,14000,0,1,3,'2019-10-29 15:37:45','2019-10-29 15:37:45'),(35,'2019-10-27','Dewi',2,5,2,70000,85000,1,6000,2,6000,0,1,3,'2019-10-29 15:38:54','2019-11-01 08:37:34'),(36,'2019-10-28','Nunung',3,4,1,35000,80000,1,0,3,20000,1,2,3,'2019-10-29 15:42:06','2019-11-09 20:53:12'),(37,'2019-10-28','Admin 1',3,4,1,35000,80000,1,0,3,0,1,2,3,'2019-10-29 15:42:44','2019-11-09 20:53:58'),(38,'2019-10-30','Elni Juniarti',4,6,4,50000,100000,1,14000,1,14000,1,2,3,'2019-10-30 23:33:27','2019-11-08 22:02:50'),(39,'2019-11-05','Nurroisyah',3,6,4,45000,100000,2,14000,1,14000,1,2,3,'2019-11-05 22:24:43','2019-11-20 05:47:59'),(40,'2019-11-06','Niken Prameshwari',1,3,3,55000,85000,1,14000,1,14000,0,2,3,'2019-11-06 12:44:54','2019-11-20 05:46:49'),(41,'2019-11-06','Niken Prameshwari',4,5,4,50000,90000,1,0,1,14000,0,2,3,'2019-11-06 12:45:57','2019-11-20 05:47:00'),(42,'2019-11-07','Wiwit',1,5,3,50000,75000,2,14000,1,14000,1,2,3,'2019-11-07 12:34:38','2019-11-20 05:50:53'),(43,'2019-11-07','Bibit Pramuwati',4,3,4,55000,90000,1,0,1,14000,0,2,3,'2019-11-07 12:39:50','2019-11-20 05:47:10'),(44,'2019-11-07','Bibit Pramuwati',4,4,4,55000,90000,1,0,1,0,0,2,3,'2019-11-07 12:40:54','2019-11-20 05:47:19'),(45,'2019-11-13','Amalia Dini',2,4,3,48000,100000,6,19000,1,14000,0,2,3,'2019-11-13 14:52:52','2019-11-20 05:48:41'),(46,'2019-11-13','Amalia Dini',2,6,3,48000,100000,1,0,1,0,0,2,3,'2019-11-13 14:55:51','2019-11-20 05:48:52'),(47,'2019-11-13','Amalia Dini',4,7,4,60000,90000,1,22000,1,22000,1,2,3,'2019-11-13 15:03:03','2019-11-20 05:49:07'),(48,'2019-11-13','Amalia Dini',4,5,4,65000,90000,2,0,1,0,1,2,3,'2019-11-13 15:05:36','2019-11-20 05:49:21'),(49,'2019-11-13','Amalia Dini',4,4,1,55000,90000,2,20000,3,20000,1,2,3,'2019-11-13 15:06:48','2019-11-20 05:49:38'),(50,'2019-11-13','Amalia Dini',4,3,4,60000,90000,5,22000,1,22000,1,2,3,'2019-11-13 15:08:18','2019-11-20 05:49:51'),(51,'2019-11-13','Amalia Dini',4,2,4,65000,90000,2,0,1,0,1,2,3,'2019-11-13 15:10:43','2019-11-20 05:50:02'),(52,'2019-11-13','Harjo',1,5,5,50000,85000,1,0,2,0,1,2,7,'2019-11-13 15:31:02','2019-11-18 14:43:59'),(53,'2019-11-13','Harjo',2,5,5,50000,100000,1,6000,2,6000,1,2,7,'2019-11-13 15:31:44','2019-11-18 14:44:09'),(54,'2019-11-18','Harjo',1,6,6,65000,85000,1,6000,2,6000,1,2,3,'2019-11-18 15:33:04','2019-11-22 10:33:59'),(55,'2019-11-18','Harjo',2,6,3,50000,100000,1,6000,2,6000,1,2,3,'2019-11-18 15:52:34','2019-11-22 10:34:13'),(56,'2019-11-19','Wiwit',1,3,1,31000,75000,1,6000,2,6000,1,2,7,'2019-11-19 13:16:25','2019-11-28 01:41:53'),(57,'2019-11-19','Wiwit',1,4,1,31000,75000,1,0,2,0,1,2,7,'2019-11-19 13:27:10','2019-11-28 01:42:12'),(58,'2019-11-19','Wiwit',1,5,1,31000,75000,1,0,2,0,1,2,7,'2019-11-19 13:30:59','2019-11-28 01:42:25'),(62,'2019-11-20','Mulyadi',4,3,4,56000,80000,1,2000,1,14000,0,2,2,'2019-11-20 19:53:47','2019-11-20 19:53:47'),(63,'2019-11-20','Mulyadi',2,6,2,80000,100000,1,0,2,8000,0,2,2,'2019-11-20 20:14:51','2019-11-20 20:14:51'),(64,'2019-11-20','Mulyadi',6,1,2,90000,105000,1,0,2,0,0,2,2,'2019-11-20 20:16:22','2019-11-20 20:16:22'),(65,'2019-11-20','Yustiva',4,4,4,55000,80000,1,22000,1,22000,1,2,5,'2019-11-20 20:20:21','2019-11-20 20:20:21'),(66,'2019-11-20','Yustiva',4,6,4,55000,55000,1,0,1,0,1,2,7,'2019-11-20 20:22:47','2019-11-20 20:22:47'),(69,'2019-11-20','Yustiva',3,5,4,50000,50000,1,0,1,0,1,2,7,'2019-11-20 20:27:27','2019-11-20 20:27:27'),(70,'2019-11-20','Dewi',3,8,4,55000,100000,1,0,1,14000,0,2,3,'2019-11-20 20:34:25','2019-12-06 02:02:40'),(71,'2019-11-21','Angel',2,6,3,55000,105000,1,6000,2,6000,0,2,3,'2019-11-21 21:33:13','2019-11-28 13:44:38'),(72,'2019-11-21','Angel',4,8,4,55000,105000,1,0,1,14000,0,2,3,'2019-11-21 21:34:48','2019-11-28 13:46:04'),(73,'2019-11-28','Yustiva',4,7,4,55000,95000,1,14000,1,14000,1,2,3,'2019-11-28 13:47:59','2019-12-06 02:02:57'),(74,'2019-12-05','Wahid Hasyim',2,6,1,50000,110000,1,6000,2,6000,1,2,3,'2019-12-06 02:00:51','2019-12-06 02:00:51'),(75,'2019-12-05','Wahid Hasyim',4,3,1,60000,100000,1,0,2,0,1,2,3,'2019-12-06 02:01:24','2019-12-06 02:01:24'),(77,'2019-12-19','Dewi',3,7,4,50,100,1,0,1,14000,0,2,1,'2019-12-26 00:27:36','2019-12-26 00:27:36'),(78,'2019-12-22','Kesyi',1,5,7,37500,85000,1,20000,3,20000,1,2,3,'2019-12-26 00:31:16','2019-12-26 00:35:48'),(79,'2019-12-22','Kesyi',1,6,7,60000,95000,1,0,3,0,1,2,3,'2019-12-26 00:32:53','2019-12-26 00:36:12'),(80,'2019-12-22','Kesyi',3,7,7,60,100,1,0,3,0,1,2,3,'2019-12-26 00:33:45','2019-12-26 00:36:28'),(81,'2019-12-22','Yustiva',1,8,7,70000,90000,1,0,1,0,1,2,3,'2019-12-26 00:37:07','2019-12-26 03:04:20'),(82,'2019-12-22','Yustiva',3,8,7,80000,100000,1,0,1,0,1,2,3,'2019-12-26 00:39:58','2019-12-26 03:04:56'),(83,'2019-12-22','Yustiva',11,1,8,60000,100000,1,0,1,0,1,2,3,'2019-12-26 00:43:18','2019-12-26 03:05:22'),(86,'2019-12-25','Wahid Hasyim',2,6,1,55000,125000,1,11000,1,31000,0,2,3,'2019-12-26 06:14:12','2019-12-26 06:14:12'),(87,'2019-12-23','Rika Andriana',1,1,2,55000,85000,3,22500,1,0,0,2,3,'2019-12-26 06:19:06','2019-12-26 06:19:22'),(88,'2019-12-23','Rika Andriana',2,1,1,75000,105000,7,52500,1,0,0,2,3,'2019-12-26 06:21:00','2019-12-26 06:21:00'),(89,'2019-12-23','Rika Andriana',4,9,2,75000,105000,6,0,1,0,0,2,3,'2019-12-26 06:26:32','2019-12-26 06:28:00'),(90,'2019-12-23','Rika Andriana',4,1,2,75000,95000,19,0,1,0,0,2,3,'2019-12-26 06:27:45','2019-12-26 06:27:45');
/*!40000 ALTER TABLE `sales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sizes`
--

DROP TABLE IF EXISTS `sizes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sizes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sizes`
--

LOCK TABLES `sizes` WRITE;
/*!40000 ALTER TABLE `sizes` DISABLE KEYS */;
INSERT INTO `sizes` VALUES (1,'All Size','2019-10-29 01:20:51','2019-10-29 01:20:51'),(2,'S','2019-10-29 01:20:56','2019-10-29 01:20:56'),(3,'M','2019-10-29 01:21:01','2019-10-29 01:21:01'),(4,'L','2019-10-29 01:21:10','2019-10-29 01:21:10'),(5,'XL','2019-10-29 01:21:18','2019-10-29 01:21:18'),(6,'XXL','2019-10-29 01:21:25','2019-10-29 01:21:25'),(7,'3XL','2019-10-29 01:21:32','2019-10-29 01:21:32'),(8,'4XL','2019-10-29 01:21:41','2019-10-29 01:21:41'),(9,'Jumbo','2019-10-29 01:21:49','2019-10-29 01:21:49');
/*!40000 ALTER TABLE `sizes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statuses`
--

DROP TABLE IF EXISTS `statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statuses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statuses`
--

LOCK TABLES `statuses` WRITE;
/*!40000 ALTER TABLE `statuses` DISABLE KEYS */;
INSERT INTO `statuses` VALUES (1,'keep','2019-10-29 01:19:13','2019-11-20 09:31:39'),(2,'onprogress','2019-10-29 01:19:23','2019-10-29 01:19:23'),(3,'done','2019-10-29 01:19:36','2019-10-29 01:19:47'),(4,'done unpaid','2019-10-29 01:20:06','2019-10-29 01:20:06'),(5,'onprogress unpaid','2019-10-29 01:20:16','2019-10-29 01:20:16'),(6,'return','2019-10-29 01:20:28','2019-10-29 01:20:28'),(7,'stock','2019-10-29 01:20:36','2019-10-29 01:20:36');
/*!40000 ALTER TABLE `statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suppliers`
--

DROP TABLE IF EXISTS `suppliers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suppliers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suppliers`
--

LOCK TABLES `suppliers` WRITE;
/*!40000 ALTER TABLE `suppliers` DISABLE KEYS */;
INSERT INTO `suppliers` VALUES (1,'Batik Kampus','08980529292','2019-10-29 01:13:52','2019-10-29 01:13:52'),(2,'Arisska Batik','085825000117','2019-10-29 01:14:31','2019-10-29 01:14:31'),(3,'Putra Fajar','085642731078','2019-10-29 01:15:07','2019-10-29 01:15:07'),(4,'Dianputri','082322425085','2019-10-29 01:15:45','2019-10-29 01:15:45'),(5,'Roovic Art','08990399933','2019-10-29 01:16:29','2019-10-29 01:16:29'),(6,'Putra Ghafur','081391171631','2019-11-18 14:43:16','2019-11-18 14:43:16'),(7,'Alvin Yudha','0','2019-12-26 00:14:35','2019-12-26 00:14:35'),(8,'Batik Sekaten','0','2019-12-26 00:44:26','2019-12-26 00:44:26');
/*!40000 ALTER TABLE `suppliers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'zainul','achmadzainulkarim@gmail.com',NULL,'$2y$10$JaKnaeO0fhvvGH9PW7e4n.Lp3z8QIfwJFUqBndRdNknTET8wTX7iO','UXbGQcPNWc2vThQvgnD5TKDZVucAn8onQpn3w7VqFgjBFELbgJooVbzh60Dq','2019-10-29 01:11:23','2019-10-29 01:11:23'),(2,'rahajeng','ruangbatikcewek@gmail.com',NULL,'$2y$10$Ic10uZiHSxg/GUuy4n5P5O0fagUEQiL7cSAO8VPdytR3599uDI4v.','lbqHuy3EOnkUNtpCXhYIMld7qEbfDrF3oZapYTU4aER2utuZqsCLzOCbluGL','2019-10-29 08:07:24','2019-10-29 08:07:24');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-26 19:39:46
