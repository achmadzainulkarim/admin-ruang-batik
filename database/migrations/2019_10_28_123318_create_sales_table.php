<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('date');
            $table->string('customer');
            $table->integer('product_id');
            $table->integer('size_id');
            $table->integer('supplier_id');
            $table->integer('buy');
            $table->integer('sell');
            $table->integer('quantity');
            $table->integer('discount')->default(0);
            $table->integer('courier_id');
            $table->integer('delivery');
            $table->integer('cod')->default(0);
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
